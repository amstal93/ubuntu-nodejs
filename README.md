# ubuntu-nodejs

A Docker image based on Ubuntu including a NodeJS install & tools.

Development tools:
 - build-essential (gcc, g++, make)
 - clang
 - cmake
 - git
 - kcov (from https://github.com/SimonKagstrom/kcov)
 - llvm
 - make
 - node-gyp
 - pkg-config
 - python-minimal

Development libraries:
 - binutils-dev
 - libclang-dev
 - libdw-dev
 - libiberty-dev
 - libjemalloc-dev
 - libpcap-dev
 - librocksdb-dev
 - libssl-dev
 - zlib1g-dev

Misc. tools:
 - debsigs
 - unzip
 - zip

## Setup

* Install docker: https://docs.docker.com/engine/installation/
* Build the docker image: `make build`
* Try the docker image: `make shell`
